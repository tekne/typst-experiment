= Baby Linear Lambda Calculus

#let dnt(term) = $[|#term|]$
#let qq = h(1cm)
#let prftree(..subtrees, name: none) = {
    let subtrees = subtrees.pos();
    let conclusion = subtrees.pop();
    $
        #text(maroon)[#name] lr(#{ subtrees.join(h(0.4cm)) } / conclusion)
    $
}
#let rule(terminal, ..cases) = $
    terminal ::= #{ cases.pos().join($ #h(0.2cm) | #h(0.2cm) $) }
$
#let abort = $sans("abort")$
#let llet = $sans("let")$
#let lin = $sans("in")$
#let lcases = $sans("cases")$
#let lproj = $sans("proj")$
#let nilc = $dot.op$
#let splits(src, ..subctx) = {
    let subctx = subctx.pos().join($;$);
    $src arrow.r.bar subctx$
}
#let tobj = $bold(0)$
#let moni = $I$

Here's our language: 
#list(
    marker: "",
    $#rule($P, Q, R$)[$A$][$bot$][$P => Q$][$P and Q$][$P or Q$]$,
    $#rule($p, q, r$)[$x$][$abort p$][$lambda x: P.p$][$q p$][$(p, q)$][$iota_i p$][$lproj$][$lcases$]$,
    $#rule($Gamma, Delta, Xi$)[$nilc$][$Gamma, x: P$]$
)
Here's our typing rules
#let rules = (
    split-nil: prftree(name: "split-nil")[$splits(nilc, nilc, nilc)$],
    left: prftree(name: "left")[$splits(Gamma, Delta, Xi)$][$splits(#[$Gamma, x: P$], #[$Delta, x: P$], Xi)$],
    right: prftree(name: "right")[$splits(Gamma, Delta, Xi)$][$splits(#[$Gamma, x: P$], Delta, #[$Xi, x: P$])$],
    var: prftree(name: "var")[$x: P models x: P$],
    abort: prftree(name: "abort")[$Gamma models p: bot$][$Gamma models abort p: P$],
    app: prftree(name: "app")[$Delta models q: P => Q$][$Xi models p: P$][$splits(Gamma, Delta, Xi)$][$Gamma models q p: Q$],
    lam: prftree(name: "lam")[$Gamma, x: P models q: Q$][$Gamma models lambda x. q: P => Q$],
    pair: prftree(name: "pair")[$Delta models p: P$][$Xi models q: Q$][$splits(Gamma, Delta, Xi)$][$Gamma models (p, q): P and Q$],
    orl: prftree(name: "orl")[$Gamma models p: P$][$Gamma models iota_0 p: P or Q$],
    orr: prftree(name: "orr")[$Gamma models q: Q$][$Gamma models iota_1 q: P or Q$],
    proj: prftree(name: "proj")[$nilc models lproj: P and Q => (P => Q => R) => R$],
    cases: prftree(name: "cases")[$nilc models lcases: P or Q => (P => R) => (Q => R) => R$]
)
$ rules.at("split-nil") qq rules.at("left") qq rules.at("right") $
$ rules.at("var") qq rules.at("abort") qq rules.at("app") $
$ rules.at("lam") qq rules.at("pair") $
$ rules.at("orl") qq rules.at("orr") $
$ rules.at("proj") $
$ rules.at("cases") $
Let $cal(C)$ be a closed symmetric monoidal category with coproducts and an initial object. Define denotations in $cal(C)$ as follows:
$
#rect($dnt(P): |cal(C)|$)
$
$
dnt(A) = A qq
dnt(bot) = tobj qq
dnt(P => Q) = dnt(Q)^(dnt(P)) qq
dnt(P and Q) = dnt(P) times.circle dnt(Q) qq
dnt(P or Q) = dnt(P) + dnt(Q)
$
$
#rect($dnt(Gamma): |cal(C)|$)
$
$
#rect($dnt(Gamma models p: P): cal(C)(dnt(Gamma), dnt(P))$)
$
$
dnt(nilc) = moni qq dnt(#[$Gamma, x: P$]) = dnt(Gamma) times.circle dnt(P)
$
$
#rect($dnt(splits(Gamma, Delta, Xi)): cal(C)(dnt(Gamma), dnt(Delta) times.circle dnt(Xi))$)
$
$
dnt(rules.at("split-nil")) = lambda_moni^(-1) qq
dnt(rules.at("left")) = dnt(splits(Gamma, Delta, Xi));alpha;(dnt(Delta) times.circle sigma)
$
$
dnt(rules.at("right")) = alpha;(dnt(splits(Gamma, Delta, Xi)) times.circle dnt(P))
$
$
#rect($dnt(Gamma models p: P): cal(C)(dnt(Gamma), dnt(P))$)
$
$
dnt(rules.at("var")) = sans("id") qq
dnt(rules.at("abort")) = dnt(Gamma models p: bot);0_P
$
$
dnt(rules.at("app"))
= ...
$
$
dnt(rules.at("lam"))
= ...
$
$
dnt(rules.at("pair")) = dnt(splits(Gamma, Delta, Xi)); (dnt(Delta models p: P) times.circle dnt(Xi models q: Q))
$
$
dnt(rules.at("orl")) = dnt(Gamma models p: P);iota_0 qq
dnt(rules.at("orr")) = dnt(Gamma models q: Q);iota_1
$
$
dnt(rules.at("proj")) = ...
$
$
dnt(rules.at("cases")) = ...
$